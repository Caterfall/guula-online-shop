from django.urls import path
from . import views

urlpatterns = [
    path('', views.guula_app, name='guula_app'),
    path('signin', views.signin, name='signin'),
    path('signup', views.signup, name='signup'),
    path('nav', views.nav, name='nav'),
]