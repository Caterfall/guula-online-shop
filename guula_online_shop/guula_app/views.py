from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader

def guula_app(request):
    template = loader.get_template('guula.html')
    return HttpResponse(template.render())
def signin(request):
    return render(request, 'signin.html')
def signup(request):
    return render(request, 'signup.html')
def nav(request):
    return render(request, 'navbr.html')