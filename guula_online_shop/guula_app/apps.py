from django.apps import AppConfig


class GuulaAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'guula_app'
